float paddel_gegner = 0;
float paddel_spieler = 0;
PVector ball;
PVector ball_richtung;

int paddel_breite = 20;
int paddel_hoehe = 150;
int ball_breite = 25;

void setup() {
  size(1024, 768, P2D);
  background(0, 32, 0);
  
  paddel_gegner = height / 2;
  paddel_spieler = height / 2;
  
  ball = new PVector(width / 2, height / 2);
  ball_richtung = PVector.random2D();
  ball_richtung.mult(5);
}

void draw() {
  background(0, 32, 0);
  
  // Paddel-Steuerung
  paddel_spieler = mouseY;
    
  rect(10, paddel_spieler - paddel_hoehe / 2, 
    paddel_breite, paddel_hoehe);
    
  rect(width - 10 - paddel_breite / 2, paddel_gegner - paddel_hoehe / 2, 
    paddel_breite, paddel_hoehe);
  
  
  // Ball-Steuerung
  if (ball.y < 0 && ball_richtung.y < 0) {
    ball_richtung.y *= -1;
  } else if (ball.y > height && ball_richtung.y > 0) {
    ball_richtung.y *= -1;
  }
  
  if (ball.x < 40 && ball_richtung.x < 0) {
    if (ball.y < paddel_spieler + paddel_hoehe / 2 && ball.y > paddel_spieler - paddel_hoehe / 2) {
      ball_richtung.x *= -1;
    }
  } else if (ball.x > width - 40 && ball_richtung.x > 0) {
    if (ball.y < paddel_gegner + paddel_hoehe / 2 && ball.y > paddel_gegner - paddel_hoehe / 2) {
      ball_richtung.x *= -1;
    }
  }
  
  
  
  ball.add(ball_richtung);
  
  rect(ball.x - ball_breite / 2, ball.y - ball_breite / 2, 
    ball_breite, ball_breite);
}
