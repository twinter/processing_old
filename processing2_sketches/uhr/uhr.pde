

void setup() {
  size(700, 700);
}

void draw() {
  background(255);
  
  for (int i = 0; i < 60; i++) {
    if (i > second()) {
      break;
    }
    line(50 + i * 10, 50, 50 + i * 10 , 100);
  }
  
  for (int i = 0; i < 60; i++) {
    if (i > minute()) {
      break;
    }
    line(50 + i * 10, 150, 50 + i * 10 , 200);
  }
}
