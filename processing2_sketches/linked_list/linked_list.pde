Blink anchor;

void setup()
{
  size(1024, 768);
  ellipseMode(RADIUS);
  frameRate(15);

  Blink tmp;
  anchor = new Blink(1000, 600, 10);
  
  for (int i = 10; i > 0; i--) 
  {
    tmp = anchor;
    anchor = new Blink(100 * i - 50, 300, 25, 2);
    anchor.appendChild(tmp);
  }
  tmp = anchor;
  anchor = new Blink(100, 100, 50, 2);
  anchor.appendChild(tmp);
}

void draw()
{
  background(128);
  anchor.render();
  anchor.step();
}


class Blink
{
  int pos_x;
  int pos_y;
  int r;
  int state = 0;
  int max_state = 1;
  color fill = color(255);
  Blink next = null;

  Blink(int x, int y, int size)
  {
    pos_x = x;
    pos_y = y;
    r = size;
  }

  Blink(int x, int y, int size, int max)
  {
    pos_x = x;
    pos_y = y;
    r = size;
    max_state = max;
  }

  void render()
  {
    fill( map(state, 0, max_state, 255, 64) );

    ellipse(pos_x, pos_y, r, r);

    if (next != null) next.render();
  }

  void step()
  {
    state++;
    if (state > max_state) {
      state = 0;
      if (next != null) next.step();
    }
  }

  void appendChild(Blink blink)
  {
    next = blink;
  }

  void removeChild()
  {
    next = null;
  }
}

