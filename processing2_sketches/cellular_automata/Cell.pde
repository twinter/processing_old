class Cell {
  /*
  
  */
  int x, y;
  color c;
  Rule rules;

  Cell(int x, int y, color c, Rule r)
  {
    this.x = x;
    this.y = y;
    this.c = c;
    rules = r;
  }
  
  void render()
  {
    set(x, y, c);
  }
  
  void update()
  {
    // run logic / move
    rules.apply(x, y, this);
    
    //fix position
    x = Helper.fixX(x, width);
    y = Helper.fixY(y, height);
    
  }
  
  boolean move(int delta_x, int delta_y)
  {
    int new_x = Helper.fixX(x + delta_x, width);
    int new_y = Helper.fixY(y + delta_y, height);
    if (get(new_x, new_y) == black)
    {
      set(x, y, black);
      x = new_x;
      y = new_y;
      set(x, y, c);
      return true;
    }
    else
    {
      return false;
    }
  }
  
}
