/*
Manages a number of condition lists.
A single list returns true on the first match but every list needs to return true 
  for the whole set to return true as well.
This class is built as a linked list. This means only the first element (anchor) ist saved
  and each element contains a reference to the next one. The last one contains null instead.
*/
class ConditionSet {
  private Condition c_anchor;
  private ConditionSet next = null;
  
  ConditionSet(Condition c_anchor) {
    this.c_anchor = c_anchor;
  }
  
  boolean check(int x, int y)
  {
    boolean result = c_anchor.check(x, y);
    if (result)
    {
      if (next != null) return next.check(x, y);
      else return true;
    }
    else
    {
      return false;
    }
  }
  
  /* Add a ConditionSet at the end of the list. */
  ConditionSet append(ConditionSet cs)
  {
    if (next == null)
    {
      next = cs;
      return this;
    }
    else
    {
      return next.append(cs);
    }
  }
  
}





/*
The conditions are built as a simple linked list,
means only the first one is saved/called called in Rule and it ripples down.
the list returns true on the first match.
The one inserted last is checked first in most cases.
*/
class Condition {
  protected Condition next = null;
  
  protected boolean check(int x, int y)
  {
    return false;
  }
  
  /* Add a Condition at the end of the list. */
  Condition append(Condition condition)
  {
    if (next == null)
    {
      next = condition;
      return this;
    }
    else
    {
      return next.append(condition);
    }
  }
  
}



class IsFullCondition extends Condition {
  private int delta_x, delta_y;
  private boolean test_full;
  
  IsFullCondition(int dx, int dy, boolean is_full)
  {
    delta_x = dx;
    delta_y = dy;
    test_full = is_full;
  }
  
  boolean check(int x, int y)
  {
    int tmp_x = Helper.fixX(x + delta_x, width);
    int tmp_y = Helper.fixY(y + delta_y, height);
    
    boolean is_full = (get(tmp_x, tmp_y) != black);
    if ( is_full == test_full )
    {
      return true;
    }
    else
    {
      if (next != null) return next.check(x, y);
      else return false;
    }
  }
  
}



class IsColorCondition extends Condition {
  private int delta_x, delta_y;
  private boolean test_full;
  private color c;
  IsColorCondition(int dx, int dy, boolean is_color, color c)
  {
    delta_x = dx;
    delta_y = dy;
    test_full = is_color;
    this.c = c;
  }
  
  boolean check(int x, int y)
  {
    int tmp_x = Helper.fixX(x + delta_x, width);
    int tmp_y = Helper.fixY(y + delta_y, height);
    
    boolean is_color = (get(tmp_x, tmp_y) == c);
    if ( is_color == test_full )
    {
      return true;
    }
    else
    {
      if (next != null) return next.check(x, y);
      else return false;
    }
  }
  
}













