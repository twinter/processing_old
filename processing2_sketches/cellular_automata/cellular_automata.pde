/*
Idee: 2D-Wolfram-CA mit einzelnem startpunkt in der Mitte
zwei Populationen erschaffen und kämpfen lassen, evtl evolutionär verbessern

*/

int maxcells = 100000;
int runs_per_loop = 50000;
Cell[] cells = new Cell[maxcells];

int numcells = 0;
color black = color(0, 0, 0);

void setup()
{
  //basic setup
  size(1024, 768, P2D);
  frameRate(60);
  background(black);
  noSmooth();
  stroke(255);
  
  
  
  //define rules
  color c_right = color(0, 255, 0);
  color c_left = color(200);
  
  
  Rule r_right = new Rule(
    new ConditionSet( new IsFullCondition(1, -1, false) )
    .append( new ConditionSet(new IsFullCondition(1, 0, false)) )
    .append( new ConditionSet(new IsFullCondition(1, 1, false)) )
    ,
    new MoveAction(1, 0)
  )
  .append(new Rule(
    new ConditionSet(
      new IsColorCondition(0, -1, true, c_left)
      .append( new IsColorCondition(-1, -1, true, c_left) )
      .append( new IsColorCondition(-1, 0, true, c_left) )
      .append( new IsColorCondition(-1, 1, true, c_left) )
      .append( new IsColorCondition(0, 1, true, c_left) )
    ),
    new MoveRandomAction(1)
  ));
  
  
  Rule r_left = new Rule(
    new ConditionSet( new IsFullCondition(-1, -1, false) )
    .append( new ConditionSet(new IsFullCondition(-1, 0, false)) )
    .append( new ConditionSet(new IsFullCondition(-1, 1, false)) )
    ,
    new MoveAction(-1, 0)
  )
  .append(new Rule(
    new ConditionSet(
      new IsColorCondition(0, -1, true, c_right)
      .append( new IsColorCondition(1, -1, true, c_right) )
      .append( new IsColorCondition(1, 0, true, c_right) )
      .append( new IsColorCondition(1, 1, true, c_right) )
      .append( new IsColorCondition(0, 1, true, c_right) )
    ),
    new MoveRandomAction(1)
  ));
  
  
  
  //place cells
  for (int i = 0; i < maxcells; i++)
  {
    int x, y;
    x = floor(random(width));
    y = floor(random(height));
    
    if (get(x, y) == black)
    {
      float rnd = random(1);
      if (rnd < 0.5)
        cells[numcells] = new Cell(x, y, c_right, r_right);
      else
        cells[numcells] = new Cell(x, y, c_left, r_left);
      cells[numcells].render();
      numcells++;
    }
  }
  
  
  println(numcells, " cells created, ", 100 * numcells / maxcells, "% succesfull tries");
  //runs_per_loop =  numcells;
}

void draw()
{
  
  for (int i = 0; i < runs_per_loop; i++)
  {
    int x = floor(random(numcells));
    cells[x].update();
  }
  
  println(frameRate);
}




















