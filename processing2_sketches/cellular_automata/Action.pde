class Action {
  void act(Cell cell) {}
}

class MoveAction extends Action {
  int delta_x, delta_y;
  
  MoveAction(int delta_x, int delta_y)
  {
    this.delta_x = delta_x;
    this.delta_y = delta_y;
  }
  
  void act(Cell c)
  {
    c.move(delta_x, delta_y);
  }
}



/*
Move the target to a random slot within a box.
This Box is defined by the distance from the target in every direction. Those distances are
  saved as a difference in the coordinates of the pixel grid. Therefore the values for
  top and left are inverted on input.
*/
class MoveRandomAction extends Action {
  int left, right, top, bottom;
  
  MoveRandomAction(int d)
  {
    left = d * -1; right = d; top = d * -1; bottom = d;
  }
  
  MoveRandomAction(int x, int y)
  {
    left = x * -1; right = x;
    top= y * -1; bottom = y;
  }
  
  MoveRandomAction(int l, int r, int t, int b)
  {
    left = l * -1;
    right = r;
    top = t * -1;
    bottom = b;
  }
  
  void act(Cell c)
  {
    int delta_x = floor(random(left, right));
    int delta_y = floor(random(top, bottom));
    c.move(delta_x, delta_y);
  }
}
















