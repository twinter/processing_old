/*
represents a rule for a cellular automaton and the corresponding action.
Able to check any amount of rules in any combination. See the comment on
  the class ConditionSet to learn how they behave.

don't add too many rules or your stack may explode.



ToDo / Anforderungen:
append() gibt letztes Element zurück (für method-chaining und höhere effektivität)
genauere Actions
*/

class Rule {
  private ConditionSet conditions;
  private Action action;
  private Rule next = null;
  
  Rule(ConditionSet conditions, Action action)
  {
    this.conditions = conditions;
    this.action = action;
  }
  
  boolean apply(int x, int y, Cell target)
  {
    if (conditions.check(x, y))
    {
      action.act(target);
      return true;
    }
    else
    {
      if (next != null) return next.apply(x, y, target);
      else return false;
    }
  }
  
  /* Add a Rule at the end of the list. */
  Rule append(Rule rule)
  {
    if (next == null) 
    {
      next = rule;
      return this;
    }
    else
    {
      return next.append(rule); 
    }
  }
  
}





    
