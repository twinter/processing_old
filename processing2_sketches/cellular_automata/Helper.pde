static class Helper {
  //helper functions
  public static int fixX(int x, int width)
  {
    while (x < 0) x += width;
    while (x >= width) x -= width;
    return x;
  }
  
  public static int fixY(int y, int height)
  {
    while (y < 0) y += height;
    while (y >= height) y -= height;
    return y;
  }
  
}
