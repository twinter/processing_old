class EntityManager {
  CollisionDetection2D cd = new CollisionDetection2D();
  private ArrayList<Entity> entities;
  private int time_last_update = 0;
  
  EntityManager() {
    entities = new ArrayList<Entity>();
    time_last_update = millis();
  }
  
  void draw() {
    // berechne Zeit in Sekunden seit letztem Update für genauere Bewegungen
    float time_diff = millis() - time_last_update;
    time_diff /= 1000.;
    time_last_update = millis();
    
    if (frameCount < 2) time_diff = 0;
    
    // Update und Kollisionserkennung
    // Schleife rückwärts durchlaufen -> Sicherheit beim Löschen von Elementen
    for (int i = entities.size() - 1; i > 0; i--) {
      Entity e1 = entities.get(i);
      e1.update(time_diff);
      
      // Test auf Kollisionen
      //cd.start();
      for (int j = i - 1; j >= 0; j--) {
        Entity e2 = entities.get(j);
        if (cd.checkCollision(e1, e2)) e1.reverseUpdate();
        
      }
      //cd.end();
      
      e1.draw();
    }
  }
  
  void add(Entity entity) {
    entities.add(entity);
  }
  
  void addMultiple(int x) {
    for (int i = 0; i < x; i++) {
      if (i % 3 == 0)
        entities.add(new Entity());
      else if (i % 3 == 1)
        entities.add(new Rectangle());
      else if (i % 3 == 2)
        entities.add(new Ellipse());
    }
  }
  
  
}
