class Ellipse extends Entity {
  PVector size;
  
  Ellipse() {
    super();
    this.size = new PVector(random(10, 100), 0);
    this.size.y = this.size.x;
  }
  
  Ellipse(PVector position, PVector force, PVector size) {
    super(position, force);
    this.size = size;
  }
  
  void draw() {
    ellipseMode(CENTER);
    ellipse(position.x, position.y, size.x, size.y);
  }
  
  float calculateBoundingBoxTop() { return this.position.y - this.size.y / 2; }
  float calculateBoundingBoxLeft() { return this.position.x - this.size.x / 2; }
  float calculateBoundingBoxBottom() { return this.position.y + this.size.y / 2; }
  float calculateBoundingBoxRight() { return this.position.x + this.size.x / 2; }
  
}
