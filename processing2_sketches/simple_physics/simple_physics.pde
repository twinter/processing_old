//simple physics (2D)
EntityManager em = new EntityManager();


void setup() {
  size(1024, 768, P2D);
  background(0);
  fill(255);
  loop();
  textSize(12);
  
  em.addMultiple(60);
}

void draw() {
  background(0);
  em.draw();
  text(frameRate, 10, 10);
}
