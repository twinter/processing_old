class CollisionDetection2D {
  float frame_top, frame_bottom, frame_left, frame_right;
  PGraphics pg;
  boolean started = false; 
  Entity last_element_1, last_element_2;
  
  CollisionDetection2D() {
    //pg = createGraphics(width, height);
  }
  
  // Sollten an Anfang bzw. Ende des Zeichnens in pg aufgerufen werden
  public void start() {
    pg.beginDraw();
    started = true;
  }
  public void end() {
    pg.endDraw();
    started = false;
  }
  
  // testet zwei Obejkte auf Kollision
  public boolean checkCollision(Entity e1, Entity e2) {
    // Konzept: updaten, testen, zurücksetzen und genauer rechnen falls positiv
    //Bounding Box-Test (grober Test)
    if (
      e1.getBoundingBoxTop() > e2.getBoundingBoxBottom() || 
      e1.getBoundingBoxLeft() > e2.getBoundingBoxRight() || 
      e1.getBoundingBoxBottom() < e2.getBoundingBoxTop() || 
      e1.getBoundingBoxRight() < e2.getBoundingBoxLeft()
    ) { 
      return false;
    }
    
    
    
    //feiner Test
    // überschneidenden Bereich der Bounding Boxes berechnen
    if (e1.getBoundingBoxTop() > e2.getBoundingBoxTop())
      frame_top = e1.getBoundingBoxTop();
    else
      frame_top = e2.getBoundingBoxTop();
    
    if (e1.getBoundingBoxBottom() < e2.getBoundingBoxBottom())
      frame_bottom = e1.getBoundingBoxBottom();
    else
      frame_bottom = e2.getBoundingBoxBottom();
      
    if (e1.getBoundingBoxLeft() > e2.getBoundingBoxLeft())
      frame_left = e1.getBoundingBoxLeft();
    else
      frame_left = e2.getBoundingBoxLeft();
      
    if (e1.getBoundingBoxRight() < e2.getBoundingBoxRight())
      frame_right = e1.getBoundingBoxRight();
    else
      frame_right = e2.getBoundingBoxRight();
    
    
    
    
    
    return true;
  }
}
