// unspezifische Klasse als Vorlage, Spezifizierung durch Vererbung
// stellt sich als Kreis mit 10px Durchmesser dar

class Entity {
  PVector position, last_position, force;
  float bounding_box_top, bounding_box_bottom, bounding_box_left, bounding_box_right;
  
  Entity() {
    this.position = new PVector(random(width), random(height));
    this.last_position = this.position.get();
    this.force = PVector.random2D();
    this.force.mult(random(25, 100));
  }
  
  Entity(PVector position, PVector force) {
    this.position = position;
    this.last_position = this.position.get();
    this.force = force;
  }
  
  void draw() {
    ellipseMode(CENTER);
    ellipse(position.x, position.y, 10, 10);
  }
  
  void update(float time_diff) {
    // Test auf Kollision mit den Rändern
    if ( (bounding_box_left < 0 && force.x < 0) || (bounding_box_right > width && force.x > 0) )
      force.x *= -1;
    if ( (bounding_box_top < 0 && force.y < 0) || (bounding_box_bottom > height && force.y > 0) )
      force.y *= -1;
    
    PVector tmp = force.get();
    tmp.mult(time_diff);
    last_position = position.get();
    position.add(tmp);
    updateBoundingBox();
  }
  
  void updateBoundingBox() {
    bounding_box_top = this.calculateBoundingBoxTop();
    bounding_box_bottom = this.calculateBoundingBoxBottom();
    bounding_box_left = this.calculateBoundingBoxLeft();
    bounding_box_right = this.calculateBoundingBoxRight();
  }
  
  void reverseUpdate() {
    position = last_position.get();
    updateBoundingBox();
  }
  
  void positionAdd(PVector x) {
    position.add(x);
  }
  
  void forceAdd(PVector x) {
    force.add(x);
  }
  
  PVector getPosition() { return this.position; }
  
  PVector getForce() { return this.force; }
  
  float calculateBoundingBoxTop() { return this.position.y - 5; }
  float calculateBoundingBoxLeft() { return this.position.x - 5; }
  float calculateBoundingBoxBottom() { return this.position.y + 5; }
  float calculateBoundingBoxRight() { return this.position.x + 5; }
  
  float getBoundingBoxTop() { return bounding_box_top; }
  float getBoundingBoxLeft() { return bounding_box_left; }
  float getBoundingBoxBottom() { return bounding_box_bottom; }
  float getBoundingBoxRight() { return bounding_box_right; }
  
}
