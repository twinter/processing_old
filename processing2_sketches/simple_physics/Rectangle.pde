class Rectangle extends Entity {
  PVector size; // entspricht der Diagonalen
  
  Rectangle() {
    super();
    this.size = new PVector(random(10, 100), 0);
    this.size.y = this.size.x;
  }
  
  Rectangle(PVector position, PVector force, PVector size) {
    super(position, force);
    this.size = size;
  }
  
  void draw() {
    rectMode(CENTER);
    rect(position.x, position.y, size.x, size.y);
  }
  
  float calculateBoundingBoxTop() { return this.position.y - this.size.y / 2; }
  float calculateBoundingBoxLeft() { return this.position.x - this.size.x / 2; }
  float calculateBoundingBoxBottom() { return this.position.y + this.size.y / 2; }
  float calculateBoundingBoxRight() { return this.position.x + this.size.x / 2; }
  
}
