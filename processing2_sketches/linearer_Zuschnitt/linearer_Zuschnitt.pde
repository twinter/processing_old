// lineare Rohstoffe alghorithmisch zuschneiden
FloatList materials, jobs, waste;

float materials_length = 20;
int dist = 20;

int start_materials = 20;
int start_jobs = 40;

void setup() {
  size(1024, 768);
  materials = new FloatList();
  jobs = new FloatList();
  waste = new FloatList();
  for (int i = 0; i < start_materials; i++)
    materials.append(materials_length);
  for (int i = 0; i < start_jobs; i++)
    jobs.append(random(materials_length));
}

void draw() {
  materials.sort();
  jobs.sort();
  
  findBiggestFit();
  
  for (int i = 0; i < materials.size(); i++)
    rect(dist, 40 + i * 5 + 1, materials.get(i), 3);
  for (int i = 0; i < jobs.size(); i++)
    rect(dist * 2 + materials_length, 40 + i * 5 + 1, jobs.get(i), 3);
}





void findBiggestFit() {
  float current_material, current_job, last_job;
  
  for (int i = 0; i < materials.size(); i++) {
    current_material = materials.get(i);
    last_job = -1;
    for (int j = 0; j < jobs.size(); j++) {
      current_job = jobs.get(j);
      if (current_job > current_material && last_job >= 0) {
        handleFit(i, j);
        return;
      } else if (last_job < 0) {
        // TODO
        waste.append(current_material);
        materials.remove(i);
        // put material to waste
      }
      last_job = current_job;
    }
  }
  
}

  
void handleFit(int material_pos, int job_pos) {
  if (materials.get(material_pos) >= jobs.get(job_pos)) {
    materials.remove(material_pos);
  } else {
    materials.sub(material_pos, jobs.get(job_pos));
    jobs.remove(job_pos);
  }
}










