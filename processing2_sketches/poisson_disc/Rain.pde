import ddf.minim.*;

class Rain {
  Minim minim;
  ArrayList<AudioPlayer> drops;
  AudioPlayer prototype;
  String path;
  int length = -1;
  
  Rain(String path, Object parent) {
    this.path = path;
    minim = new Minim(parent);
    drops = new ArrayList<AudioPlayer>();
  }
  
  void update() {
    for (int i = drops.size() - 1; i >= 0; i--) {
      if ( drops.get(i).position() >= length - 10 ) {
        drops.get(i).close();
        drops.remove(i);
      }
    }
  }
  
  void addDrop() {
    int drops_amnt = drops.size();
    
    AudioPlayer drop = minim.loadFile(path);
    if (length < 0) length = drop.length();
    drops.add(drop);
    //drop.setVolume(random(0.5, 1));
    drops.get(drops_amnt).setPan(random(-1, 1));
    drops.get(drops_amnt).play(0);
  }
  
  int getDropsAmnt() {
    return drops.size();
  }
  
}
