ArrayList<PVector> test = new ArrayList<PVector>();
ArrayList<PVector> bekannt = new ArrayList<PVector>();

Rain rain;

float d = 1.5; //Durchmesser der Punkte
float d_min = 75; //Mindestabstand neuer Punkte
float d_max = d_min * 1.25; //Maximalabstand neuer Punkte
final float BG = 0;
final float PUNKT = 255;
final float KREIS = BG;

final int MAX_ITEMS = 10000;

void setup() {
  size(1024, 768, P2D);
  //size(1920, 1080, P2D);
  background(BG);
  noStroke();
  ellipseMode(CENTER);
  frameRate(240);
  
  rain = new Rain("drop2.wav", this);
  
  test.add(new PVector(random(width), random(height)));
  bekannt.add(test.get(0));
  fill(KREIS);
  //ellipse(width / 2, height / 2, d_min, d_min);
  fill(PUNKT);
  ellipse(test.get(0).x, test.get(0).y, d, d);
}

void draw() {
  rain.update();
  println(frameCount + " d_min:" + d_min + " bekannt:" + bekannt.size() + " test:" + test.size() + " drops:" + rain.getDropsAmnt() + " fps:" + frameRate);
  
  int i = 20;
  
  //zufälliges Element aus Liste holen
  int e = floor(random(test.size()));
  PVector punkt = test.get( e );
  
  while (i > 0) {
    //Kopie von punkt erzeugen
    PVector punkt2 = punkt.get();
    
    //zufällige Richtung (=zufälligen Einheitsvektor erzeugen)
    PVector richtung = PVector.random2D();
    
    //zufällige Länge
    richtung.mult(random(d_min, d_max));
  
    //punkt2 auf position schieben
    punkt2.add(richtung);
    
    //Test ob neuer Punkt gültig ist
    boolean fail = false;
    
    if (punkt2.x < 0 || punkt2.x > width)
      fail = true;
    else if (punkt2.y < 0 || punkt2.y > height)
      fail = true;
    
    if (!fail) {
      for (int j = 0; j < bekannt.size(); j++) {
        if (PVector.dist(punkt2, bekannt.get(j)) < d_min) {
          fail = true;
          break;
        }
      }
    }
    
    if (!fail) {
      fill(KREIS);
      //ellipse(punkt2.x, punkt2.y, d_min, d_min);
      fill(PUNKT);
      ellipse(punkt2.x, punkt2.y, d, d);
      test.add(punkt2);
      bekannt.add(punkt2);
      
      rain.addDrop();
      
      break;
    }
    
    i--;
  }
  
  if (i <= 0) {
     test.remove(e);
     if (test.size() == 0) {
       //Radien verringern
       d_min *= 0.75;
       d_max *= 0.75;
       //d *= 0.75;
       //test wieder füllen
       for (int j = 0; j < bekannt.size(); j++)
         test.add(bekannt.get(j));
     }
  }
  
}
