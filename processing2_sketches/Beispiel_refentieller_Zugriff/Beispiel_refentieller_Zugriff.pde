void setup() {
  int x1, x2;
  String s1, s2;
  
  PVector v1, v2;
  
  // weitergabe einer Kopie von x1
  x1 = 0;
  x2 = x1;
  println("1 int: x1: " + x1 + ", x2: " + x2);
  x2 = 50;
  println("2 int: x1: " + x1 + ", x2: " + x2);
  
  // weitergabe einer Referenz auf das Objekt in v1
  v1 = new PVector(1, 1);
  v2 = v1;
  println("1 Vektor: v1: " + v1 + ", v2: " + v2);
  v2.set(50, 50);
  println("2 Vektor: v1: " + v1 + ", v2: " + v2);
}
