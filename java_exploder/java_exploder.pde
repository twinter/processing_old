/*
java_exploder
-circles of different size
-circles explode on contact with another in a number of smaller circles depending on the speed of both relative to each other
-circles under a specified size disappear
(-circles constantly decraese in diameter?)
*/
ArrayList<Circle> circles = new ArrayList<Circle>();
float gravity = 0.005;

void setup() {
	size(1024, 768, P2D);
	fill(255);
	stroke(0);
	strokeWeight(2);
	circles.add(new Circle());
	circles.add(new Circle());
	circles.add(new Circle());
}

void draw() {
	background(255);
	println(circles.size(), " circles, ", frameCount, " frames so far, ", frameRate, " fps");
	for (int i = 0; i < circles.size(); i++) {
		circles.get(i).print();
	}
}

void mousePressed() {
	if (mouseButton == LEFT) {
		boolean insideCircle = false;
		for (int i = 0; i < circles.size(); i++) {
			if (circles.get(i).isInside(mouseX, mouseY)) {
				circles.get(i).drag();
				insideCircle = true;
				break;
			}
		}
		if (!insideCircle) circles.add(new Circle(mouseX, mouseY, random(30, 70)));
	} else if (mouseButton == RIGHT) {
		for (int i = 0; i < circles.size(); i++) {
			if (circles.get(i).isInside(mouseX, mouseY)) {
				circles.remove(i);
				break;
			}
		}
	}
}



class Circle {
	//float force_scaling = 0.01;
	PVector center;
	float radius;
	PVector speed; //in pixels per millisecond
	float speed_max = 1;
	int last_update = 0;
	boolean is_dragged = false;
	
	Circle() {
		radius = random(30, 70);
		center = new PVector(random(0+radius, width-radius), random(0+radius, height-radius));
		speed = PVector.random2D();
		speed.mult(random(0.1));
		last_update = millis();
	}
	
	Circle(float center_x, float center_y, float radius) {
		center = new PVector(center_x, center_y);
		this.radius = radius;
		speed = PVector.random2D();
		speed.mult(random(0.1));
		last_update = millis();
	}
	
	Circle(float center_x, float center_y, float radius, float force_x, float force_y) {
		center = new PVector(center_x, center_y);
		this.radius = radius;
		speed = new PVector(force_x, force_y);
		last_update = millis();
	}
	
	private void update() {
		if (center.x - radius < 0) {
			speed.x = abs(speed.x);
		} else if (center.x + radius > width) {
			speed.x = abs(speed.x) * -1;
		}
		if (center.y - radius < 0) {
			speed.y = abs(speed.y);
		} else if (center.y + radius > height) {
			speed.y = abs(speed.y) * -1;
		}
		center.x += speed.x * (millis() - last_update);
		center.y += speed.y * (millis() - last_update);
		speed.y += gravity;
		last_update = millis();
	}
	
	void print() {
		if (is_dragged) {
			if (mousePressed) {
				last_update = millis();
				ellipse(mouseX, mouseY, radius * 2, radius * 2);
				center.x = mouseX;
				center.y = mouseY;
			} else {
				is_dragged = false;
				speed.x = (mouseX - center.x) / (millis() - last_update);
				speed.y = (mouseY - center.y) / (millis() - last_update);
				center.x = mouseX;
				center.y = mouseY;
				last_update = millis();
			}
		} else {
			this.update();
			ellipse(center.x, center.y, radius * 2, radius * 2);
		}
	}
	
	PVector getCenter() { return center; }
	float getRadius() { return radius; }
	boolean isInside(float x, float y) { return (center.dist(new PVector(x, y)) < radius) ? true : false;}
	void drag() { is_dragged = true; }
}