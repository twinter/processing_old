Mover[] movers;

void setup() {
  size(1024, 768, P2D);
  //fullScreen(P2D, 1);
  background(255);
  fill(0);
  noStroke();
  ellipseMode(RADIUS);
  
  movers = new Mover[500];
  for (int i=0; i < movers.length; i++) { //<>//
    movers[i] = new Mover();
  }
}

void draw() {
  println(frameRate);
  background(255);
  for (int i=0; i < movers.length; i++) {
    movers[i].update();
  }
}

void mouseClicked() {
  for (int i=0; i < movers.length; i++) {
    movers[i].addRandomTarget();
  }
}