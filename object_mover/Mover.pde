class Mover {
  PVector position;
  ArrayList<Target> targets;
  float radius;
  color c;
  boolean loop = true;
  int target_id = 0;
  int time_target = 0;
  int time_last = 0;


  Mover() {
    init(
      new PVector(random(width), random(height)), 
      random(10), 
      color(random(255))
      );
  }

  Mover(float x, float y, float radius, color c) {
    init(new PVector(x, y), radius, c);
  }

  private void init(PVector position, float radius, color c) {
    this.position = position;
    targets = new ArrayList<Target>();
    //targets.add(new Target(position, 500));
    this.radius = radius;
    this.c = c;
    time_target = millis();
    time_last = millis();
  }

  void update() {    
    if (targets.size() > 0) { //<>//
      Target t = targets.get(target_id);
      if (position.x != t.getX() || position.y != t.getY()) {
        //position not yet reached -> move
        moveToTarget(t);
      } else {
        //position reached -> next target
        switchTarget();
      }
    } else {
      time_target = millis();
    }

    //draw
    fill(c); //<>//
    ellipse(position.x, position.y, radius, radius);

    time_last = millis();
  }

  void addTarget(Target target) {
    targets.add(target);
    if (targets.size() <= 1)
      time_target = millis() + target.getTimeDelta();
  }

  void addRandomTarget() {
    this.addTarget(new Target());
  }

  void setLoop(boolean loop) {
    this.loop = loop;
  }

  // --- HELPERS ---

  private void moveToTarget(Target t) {
    //get movement vector
    PVector p_delta = PVector.sub(t.getPosition(), position);

    //limit the movement vector
    if (time_target > millis()) {
      float target_dist = p_delta.mag();
      target_dist = map(millis(), time_last, time_target, 0, target_dist);
      p_delta.limit(target_dist);
    }

    position.add(p_delta);
  }

  private void switchTarget() {
    //advance target_id to point to the next target
    if (loop) {
      target_id++;
    } else {
      if (targets.size() > 0)
        targets.remove(target_id);
    }

    //normalize target_id
    if (target_id >= targets.size()) {
      target_id = 0;
    }
    if (targets.size() > 0) {
      time_target = millis() + targets.get(target_id).getTimeDelta();
    }
  }
}