class Target {
  PVector position;
  int time_delta;
  
  Target() {
    position = new PVector(random(width), random(height));
    time_delta = (int) random(2500, 5000);
  }
  
  Target(PVector position, int time_delta) {
    this.position = position;
    this.time_delta = time_delta;
  }
  
  Target(float x, float y, int time_delta) {
    position = new PVector(x, y);
    this.time_delta = time_delta;
  }
  
  float getX() {
    return position.x;
  }
  
  float getY() {
    return position.y;
  }
  
  PVector getPosition() {
    return position;
  }
  
  void setPosition(float x, float y) {
    position = new PVector(x, y);
  }
  
  int getTimeDelta() {
    return time_delta;
  }
  
  void setTimeDelta(int time_delta) {
    this.time_delta = time_delta;
  }
  
}