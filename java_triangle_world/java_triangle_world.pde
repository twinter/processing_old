/**
 * TriangleWorld
 *
 * A random world made out of triangles. Lighting according to mouse position.
 * 
 * Controls:
 * t - toggle all movement (== stop timer)
 * q - decrease timer
 * e - increase timer
 * l - toggle light movement
 * c - toggle color
 * a/<- - move camera to the left
 * d/-> - move camera to the right
 * s/v  - lower camera
 * w/^  - heighten camera
 * mousewheel - lower/heighten the light
 */

//general settings
float triangle_width = 20; // width of one triangle in pixels
float triangle_height = 0.0; // for regular triangles: 0.0
float max_height = 50; //maximum height of any point in the landscape
float border_top = -50.0;
float border_left = -50.0;
float border_bottom = -80.0;
float border_right = -75.0;
//int window_width = 925; int window_height = 1150;
int window_width = 1900; int window_height = 1170;
//int window_width = 925; int window_height = 1150;

//toggles
boolean animate = true; // bound to space
boolean animate_landscape = true; // bound to t
boolean animate_light = true; // bound to l
boolean disable_color = true; // bound to c

//step sizes for manual changes
float vert_angle_step = HALF_PI / 100; // bound to W/S; vertical angle ("height")
float hor_angle_step = TAU / 360.0; // bound to A/D; horizontal angle ("rotation")
int time_step = 100; // bound to Q/E
int z_light_step = 10; // bound to mousewheel (away from you to lower the sun); height of the light

//influence of noise()
float noise_step = 0.000001; // x/y-scaling to use in noise()
float noise_time_step = 0.00025; // scales the time used in noise()
// influence of noise() (depending on coordinate) on color distribution, 0 or lower to disable color
float noise_color_step = 0.025;
//float noise_color_step = 0.0;

//initial values
float sim_vert_angle = HALF_PI*0.3; // vertical viewing angle in radians [HALF_PI, 0), HALF_PI is from the top
float sim_hor_angle = 0; // horizontal viewing angle in radians [0,TAU], 0 is upwards on the screen
float x_light = -1.0; // position of the light, -1.0 for center
float y_light = -1.0;
float z_light = 200.0;

//advanced settings
float max_lighted_angle = 0.5; // percentage of a half circle to light (everything beyond will be black)
float anim_light_speed = 100; // movement speed of the light



//these are all overwritten
PGraphics pg;
int time = 10;
float[][][] points; // syntax: [row][col][x|y|height|x displaced|y displaced]
int rows = 10;
int cols = 10;
int time_delta = 0;
int time_delta_landscape = 0;
int last_frame_time = 0;
boolean redraw = false;

void setup() {
  size(800, 600, P2D);
  surface.setResizable(true);
  surface.setSize(window_width, window_height);
  
  pg = createGraphics(window_width, window_height, P2D);

  frameRate(60);
  //update triangle height
  if (triangle_height==0.0) {
    triangle_height=sqrt(triangle_width*triangle_width - (triangle_width*0.5)*(triangle_width*0.5));
  }
  //center light if x/y_light==-1.0
  if (x_light==-1.0) {
    x_light = width * 0.5;
  }
  if (y_light==-1.0) {
    y_light = height * 0.5;
  }
  updateRowsAndCols();
  points = new float[rows][cols][5];
  updatePoints();
}

void draw() {
  line(10, 10, width, height); // simple fps-counter
  int tmp_time = millis();
  println(1000/(tmp_time - last_frame_time) + "fps " + frameCount);
  last_frame_time = tmp_time;
  
  doAnimations();
  if (animate || redraw || mousePressed) {
    background(0);
    noStroke();
    fill(255);
    pg.beginDraw();
    pg.background(0);
    pg.fill(255);
    pg.noStroke();
    drawTriangles();
    pg.endDraw();
    image(pg, 0, 0);
  }
}



void updatePoints() {
  if (animate && animate_landscape) {
    time = millis() - time_delta - time_delta_landscape;
  }
  float noise_time = time * noise_time_step;
  for (int row = 0; row < rows; row++) {
    float row_shift = border_left;
    float row_height = border_top + triangle_height * row;
    if (row%2==1) {
      row_shift += triangle_width*0.5;
    }
    for (int col = 0; col < cols; col++) {
      points[row][col][0] = row_shift + triangle_width * col;                          // x
      points[row][col][1] = row_height;                                                // y
      points[row][col][2] = noise(points[row][col][0], row_height, noise_time) * max_height;// z
      float displacement = points[row][col][2] / tan(sim_vert_angle);
      points[row][col][3] = points[row][col][0] - displacement * sin(sim_hor_angle);  // x displaced
      points[row][col][4] = points[row][col][1] - displacement * cos(sim_hor_angle);  // y displaced
    }
  }
}

void decideColor(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3) {
  float x_lv = x_light - ((x1 + x2 + x3) / 3); // vector from lightsource to the middle of the plane
  float y_lv = y_light - ((y1 + y2 + y3) / 3);
  float z_lv = z_light - ((z1 + z2 + z3) / 3);
  float[] normal = calculateNormal(x1, y1, z1, x2, y2, z2, x3, y3, z3);
  float length_normal = sqrt((normal[0] * normal[0]) + (normal[1] * normal[1]) + (normal[2] * normal[2]));
  float length_lv = sqrt((x_lv * x_lv) + (y_lv * y_lv) + (z_lv * z_lv));
  float dot_product = dotProduct(normal[0], normal[1], normal[2], x_lv, y_lv, z_lv);
  float angle = acos(dot_product/(length_normal * length_lv));
  float intensity = min(100, max(0, 100 - (angle / PI + max_lighted_angle) * 100));

  if (disable_color) {
    pg.fill(intensity * 2.55);
  } 
  else {
    //TODO: select part of color-spectrm w/ noise(), the selection in this band w/ another noise()
    float hue = noise(((z1 + z2 + z3) / 3) * noise_color_step, time * noise_time_step) * 360;
    pg.colorMode(HSB, 360, 100, 100);
    pg.fill(hue, 100, intensity);
  }
}

// calculates the upwards facing normal of a plane given by 3 points (implements the cross product)
float[] calculateNormal(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3) {
  float[] r = new float[3];

  r[0] = ((y1 - y2) * (z1 - z3)) - ((z1 - z2) * (y1 - y3));
  r[1] = ((z1 - z2) * (x1 - x3)) - ((x1 - x2) * (z1 - z3));
  r[2] = ((x1 - x2) * (y1 - y3)) - ((y1 - y2) * (x1 - x3));

  if (r[2] < 0.0) {
    r[0] *= -1;
    r[1] *= -1;
    r[2] *= -1;
  }
  return r;
}

float dotProduct(float x1, float y1, float z1, float x2, float y2, float z2) {
  float r = (x1 * x2) + (y1 * y2) + (z1 * z2);
  return r;
}

void doAnimations() {
  if (animate_landscape && animate) {
    updatePoints();
  }

  if ((animate_light && animate) || mousePressed) {
    float x_light_force, y_light_force;
    if (!mousePressed) {
      x_light_force = noise(1, time * noise_time_step) - 0.5;
      y_light_force = noise(9, time * noise_time_step) - 0.5;
      x_light_force += ((width - (x_light * 2)) / (2 * width)) * 0.75;
      y_light_force += ((height - (y_light * 2)) / (2 * height)) * 0.75;
    } 
    else {
      float distance = dist(mouseX, mouseY, x_light, y_light);
      x_light_force = (mouseX - x_light) / (distance + anim_light_speed); // get vetor of a little less than length 1
      y_light_force = (mouseY - y_light) / (distance + anim_light_speed);
    }
    x_light += x_light_force * anim_light_speed;
    y_light += y_light_force * anim_light_speed;
    if (!animate && mousePressed) {redraw = true;}
  }
}

void drawTriangles() {
  float x1, y1, x2, y2, x3, y3; // coordinates to return
  float cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3; // original coordinates to determine the color
  for (int row = 1; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      cx1 = points[row][col][0];
      cy1 = points[row][col][1];
      cz1 = points[row][col][2];
      x1 = points[row][col][3];
      y1 = points[row][col][4];

      //first triangle (lower left)
      if (col!=0) {
        cx2 = points[row][col-1][0];
        cy2 = points[row][col-1][1];
        cz2 = points[row][col-1][2];
        x2 = points[row][col-1][3];
        y2 = points[row][col-1][4];
        if (row%2==1) { // if in a shifted row
          cx3 = points[row-1][col][0];
          cy3 = points[row-1][col][1];
          cz3 = points[row-1][col][2];
          x3 = points[row-1][col][3];
          y3 = points[row-1][col][4];
        } 
        else {
          cx3 = points[row-1][col-1][0];
          cy3 = points[row-1][col-1][1];
          cz3 = points[row-1][col-1][2];
          x3 = points[row-1][col-1][3];
          y3 = points[row-1][col-1][4];
        }
        decideColor(cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3);
        pg.triangle(x1, y1, x2, y2, x3, y3);
      }

      //second triangle (upper right)

      cx2 = points[row-1][col][0];
      cy2 = points[row-1][col][1];
      cz2 = points[row-1][col][2];
      x2 = points[row-1][col][3];
      y2 = points[row-1][col][4];
      if (row%2==0) { // if not in a shifted row
        if (col!=0) {
          cx3 = points[row-1][col-1][0];
          cy3 = points[row-1][col-1][1];
          cz3 = points[row-1][col-1][2];
          x3 = points[row-1][col-1][3];
          y3 = points[row-1][col-1][4];
          decideColor(cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3);
          pg.triangle(x1, y1, x2, y2, x3, y3);
        }
      } 
      else if (col<=cols-2) {
        cx3 = points[row-1][col+1][0];
        cy3 = points[row-1][col+1][1];
        cz3 = points[row-1][col+1][2];
        x3 = points[row-1][col+1][3];
        y3 = points[row-1][col+1][4];
        decideColor(cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3);
        pg.triangle(x1, y1, x2, y2, x3, y3);
      }
    }
  }
}

void updateRowsAndCols() {
  rows = ceil((height - (border_top + border_bottom))/triangle_height);
  cols = floor((width - (border_left + border_right))/triangle_width);
}

void stepParameter(char parameter, int direction) {
  switch(parameter) {
  case 'v':
    if ((direction > 0) && (sim_vert_angle <= HALF_PI - vert_angle_step)) {
      sim_vert_angle += vert_angle_step;
    } 
    else if ((direction < 0) && (sim_vert_angle > vert_angle_step)) {
      sim_vert_angle -= vert_angle_step;
    }
    break;
  case 'h':
    if (direction > 0) {
      sim_hor_angle += hor_angle_step;
      if (sim_hor_angle>TAU) {
        sim_hor_angle -= TAU;
      }
    } 
    else if (direction < 0) {
      sim_hor_angle -= hor_angle_step;
      if (sim_hor_angle<0) {
        sim_hor_angle += TAU;
      }
    }
    break;
  case 't':
    if (direction > 0) {
      if (animate) {time_delta -= time_step;}
      else {time += time_step;}
    } 
    else if (direction < 0) {
      if (animate) {time_delta += time_step;}
      else {time -= time_step;}
    }
    break;
  }
  updatePoints();
  if (!animate) {
    redraw = true;;
  }
}

void mouseWheel(MouseEvent event) {
  z_light += event.getAmount() * z_light_step;
  if (!animate) {
    redraw = true;
  }
}

void keyPressed() {
  if (key==CODED) {
    if (keyCode==UP) {
      stepParameter('v', 1);
    }
    else if (keyCode==DOWN) {
      stepParameter('v', -1);
    }
    if (keyCode==RIGHT) {
      stepParameter('h', 1);
    }
    else if (keyCode==LEFT) {
      stepParameter('h', -1);
    }
  } 
  else {
    switch(key) {
    case 'w': 
      stepParameter('v', -1); 
      break;
    case 's': 
      stepParameter('v', 1); 
      break;
    case 'a': 
      stepParameter('h', -1); 
      break;
    case 'd': 
      stepParameter('h', 1); 
      break;
    case 'q': 
      stepParameter('t', -1); 
      break;
    case 'e': 
      stepParameter('t', 1); 
      break;
    case 'c': 
      disable_color = !disable_color;
      break;
    case 't':
      if (!animate_landscape) {time_delta_landscape = millis() - time - time_delta;}
      animate_landscape = !animate_landscape;
      break;
    case 'l': 
      animate_light = !animate_light;
      break;
    case ' ': 
      if (!animate) {time_delta = millis() - time;}
      animate = !animate;
      break;
    }
  }
}