import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class java_triangle_world extends PApplet {

/**
 * TriangleWorld
 *
 * A random world made out of triangles. Lighting according to mouse position.
 */

float triangle_width = 25; // width of one triangle in pixels
float triangle_height = 0.0f; // for regular triangles: 0.0

float noise_step = 0.0001f; // x/y-scaling to use in noise()
float noise_time_step = 0.00025f; // scales the time used in noise()

//step sizes for manual changes
int vert_angle_steps = 100; // number of steps in the allowed intervall
int hor_angle_steps = 360;
int time_step = 50;

int time = 10;
float sim_vert_angle = HALF_PI*0.3f; // vertical viewing angle in radians [HALF_PI, 0), HALF_PI is from the top
float sim_hor_angle = 0; // horizontal viewing angle in radians [0,TAU], 0 is upwards on the screen
float border_top = -50.0f;
float border_left = -50.0f;
float border_bottom = -80.0f;
float border_right = -75.0f;
float x_light = -1.0f; // position of the light, -1.0 for center
float y_light = -1.0f;
float z_light = 100.0f;
float max_lighted_angle = 0.5f; // percentage of a half circle to light (everything with a bigger angle will be black)
float max_height = 50; //maximum height of any point in the landscape

boolean animate = true;
boolean animate_time = true;
boolean animate_light = true;
float anim_light_time_scaling = 0.5f; // scales the speed of time used in noise() to move the light (lower->calmer)
float anim_light_speed = 100;



//these are all overwritten
float[][][] points; // syntax: [row][col][x|y|height|x displaced|y displaced]
int rows = 10;
int cols = 10;
int time_delta = 0;
int last_frame_time = 0;

/*
Dreiecke zeichnen {
 H\u00f6he der Punkte zuweisen
 perspektivische Verschiebung ausf\u00fchren
 Beleuchtung/Hellikeit der Dreiecke berechnen
 
 ? Abnahme des Lichtst\u00e4rke mit Entfernung
 }
 */

public void setup() {
  //size(925, 1150);
  //size(1900, 1170);
  size(1920, 1200);
  frameRate(60);
  //update triangle height
  if (triangle_height==0.0f) {
    triangle_height=sqrt(triangle_width*triangle_width - (triangle_width*0.5f)*(triangle_width*0.5f));
  }
  //center light if light_x/y==-1.0
  if (x_light==-1.0f) {
    x_light = width * 0.5f;
  }
  if (y_light==-1.0f) {
    y_light = height * 0.5f;
  }
  updateRowsAndCols();
  points = new float[rows][cols][5];
  updatePoints();
  //noLoop();
}

public void draw() {
  background(0);
  //stroke(0);
  noStroke();
  fill(255);
  line(10, 10, width, height);
  int tmp_time = millis();
  println(1000/(tmp_time - last_frame_time) + "fps " + frameCount);
  last_frame_time = tmp_time;
  if (animate) {doAnimations();}
  drawTriangles();
}



public void updatePoints() {
  if (animate && animate_time) {
    time = millis() - time_delta;
  }
  float noise_time = time * noise_time_step;
  for (int row = 0; row < rows; row++) {
    float row_shift = border_left;
    float row_height = border_top + triangle_height * row;
    if (row%2==1) {
      row_shift += triangle_width*0.5f;
    }
    for (int col = 0; col < cols; col++) {
      points[row][col][0] = row_shift + triangle_width * col;                          // x
      points[row][col][1] = row_height;                                                // y
      points[row][col][2] = noise(points[row][col][0], row_height, noise_time) * max_height;// z
      float displacement = points[row][col][2] / tan(sim_vert_angle);
      points[row][col][3] = points[row][col][0] - displacement * sin(sim_hor_angle);  // x displaced
      points[row][col][4] = points[row][col][1] - displacement * cos(sim_hor_angle);  // y displaced
    }
  }
}

public void decideColor(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3) {
  float x_lv = x_light - ((x1 + x2 + x3) / 3); // vector from lightsource to the middle of the plane
  float y_lv = y_light - ((y1 + y2 + y3) / 3);
  float z_lv = z_light - ((z1 + z2 + z3) / 3);
  float[] normal = calculateNormal(x1, y1, z1, x2, y2, z2, x3, y3, z3);
  float length_normal = sqrt((normal[0] * normal[0]) + (normal[1] * normal[1]) + (normal[2] * normal[2]));
  float length_lv = sqrt((x_lv * x_lv) + (y_lv * y_lv) + (z_lv * z_lv));
  float dot_product = dotProduct(normal[0], normal[1], normal[2], x_lv, y_lv, z_lv);
  float angle = acos(dot_product/(length_normal * length_lv));
  fill(min(255, max(0, 255 - (angle / PI + max_lighted_angle) * 255)) );
}

// calculates the upwards facing normal of a plane given by 3 points (implements the cross product)
public float[] calculateNormal(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3) {
  float[] r = new float[3];
  float u1 = x1 - x2;
  float u2 = y1 - y2;
  float u3 = z1 - z2;
  float v1 = x1 - x3;
  float v2 = y1 - y3;
  float v3 = z1 - z3;

  r[0] = (u2 * v3) - (u3 * v2);
  r[1] = (u3 * v1) - (u1 * v3);
  r[2] = (u1 * v2) - (u2 * v1);

  if (r[2] < 0.0f) {
    r[0] *= -1;
    r[1] *= -1;
    r[2] *= -1;
  }
  return r;
}

public float dotProduct(float x1, float y1, float z1, float x2, float y2, float z2) {
  float r = (x1 * x2) + (y1 * y2) + (z1 * z2);
  return r;
}

public void doAnimations() {
  if (animate_time) {
    updatePoints();
  }
  
  if (animate_light && !mousePressed) {
    float x_anim_light_force = noise(1, time * noise_time_step * anim_light_time_scaling) - 0.5f;
    float y_anim_light_force = noise(9, time * noise_time_step * anim_light_time_scaling) - 0.5f;
    x_anim_light_force += pow((width*0.5f - x_light) / (width*0.5f), 1);
    y_anim_light_force += pow((height*0.5f - y_light) / (height*0.5f), 1);
    x_light += x_anim_light_force * anim_light_speed;
    y_light += y_anim_light_force * anim_light_speed;
  }
}

public void drawTriangles() {
  float x1, y1, x2, y2, x3, y3; // coordinates to return
  float cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3; // original coordinates to determine the color
  for (int row = 1; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      cx1 = points[row][col][0];
      cy1 = points[row][col][1];
      cz1 = points[row][col][2];
      x1 = points[row][col][3];
      y1 = points[row][col][4];

      //first triangle (lower left)
      if (col!=0) {
        cx2 = points[row][col-1][0];
        cy2 = points[row][col-1][1];
        cz2 = points[row][col-1][2];
        x2 = points[row][col-1][3];
        y2 = points[row][col-1][4];
        if (row%2==1) { // if in a shifted row
          cx3 = points[row-1][col][0];
          cy3 = points[row-1][col][1];
          cz3 = points[row-1][col][2];
          x3 = points[row-1][col][3];
          y3 = points[row-1][col][4];
        } 
        else {
          cx3 = points[row-1][col-1][0];
          cy3 = points[row-1][col-1][1];
          cz3 = points[row-1][col-1][2];
          x3 = points[row-1][col-1][3];
          y3 = points[row-1][col-1][4];
        }
        decideColor(cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3);
        triangle(x1, y1, x2, y2, x3, y3);
      }

      //second triangle (upper right)

      cx2 = points[row-1][col][0];
      cy2 = points[row-1][col][1];
      cz2 = points[row-1][col][2];
      x2 = points[row-1][col][3];
      y2 = points[row-1][col][4];
      if (row%2==0) { // if not in a shifted row
        if (col!=0) {
          cx3 = points[row-1][col-1][0];
          cy3 = points[row-1][col-1][1];
          cz3 = points[row-1][col-1][2];
          x3 = points[row-1][col-1][3];
          y3 = points[row-1][col-1][4];
          decideColor(cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3);
          triangle(x1, y1, x2, y2, x3, y3);
        }
      } 
      else if (col<=cols-2) {
        cx3 = points[row-1][col+1][0];
        cy3 = points[row-1][col+1][1];
        cz3 = points[row-1][col+1][2];
        x3 = points[row-1][col+1][3];
        y3 = points[row-1][col+1][4];
        decideColor(cx1, cy1, cz1, cx2, cy2, cz2, cx3, cy3, cz3);
        triangle(x1, y1, x2, y2, x3, y3);
      }
    }
  }
}

public void updateRowsAndCols() {
  rows = ceil((height - (border_top + border_bottom))/triangle_height);
  cols = floor((width - (border_left + border_right))/triangle_width);
}

public void stepParameter(char parameter, int direction) {
  switch(parameter) {
  case 'v':
    if ((direction > 0) && (sim_vert_angle <= HALF_PI - HALF_PI/vert_angle_steps)) {
      sim_vert_angle += HALF_PI / vert_angle_steps;
    } 
    else if ((direction < 0) && (sim_vert_angle > HALF_PI/vert_angle_steps)) {
      sim_vert_angle -= HALF_PI / vert_angle_steps;
    }
    break;
  case 'h':
    if (direction > 0) {
      sim_hor_angle += TAU / hor_angle_steps;
      if (sim_hor_angle>TAU) {
        sim_hor_angle -= TAU;
      }
    } 
    else if (direction < 0) {
      sim_hor_angle -= TAU / hor_angle_steps;
      if (sim_hor_angle<0) {
        sim_hor_angle += TAU;
      }
    }
    break;
  case 't':
    if (direction > 0) {
      time += time_step;
    } 
    else if (direction < 0) {
      time -= time_step;
    }
    break;
  }
  updatePoints();
  if (!animate) {
    redraw();
  }
}

public void mousePressed() {
  x_light = mouseX;
  y_light = mouseY;
  if (!animate) {
    redraw();
  }
}

public void mouseDragged() {
  x_light = mouseX;
  y_light = mouseY;
  if (!animate) {
    redraw();
  }
}

public void mouseWheel(MouseEvent event) {
  z_light += event.getAmount();
  if (!animate) {
    redraw();
  }
}

public void keyPressed() {
  if (key==CODED) {
    if (keyCode==UP) {
      stepParameter('v', 1);
    }
    else if (keyCode==DOWN) {
      stepParameter('v', -1);
    }
    if (keyCode==RIGHT) {
      stepParameter('h', 1);
    }
    else if (keyCode==LEFT) {
      stepParameter('h', -1);
    }
  } 
  else {
    switch(key) {
    case 'w': 
      stepParameter('v', -1); 
      break;
    case 's': 
      stepParameter('v', 1); 
      break;
    case 'a': 
      stepParameter('h', 1); 
      break;
    case 'd': 
      stepParameter('h', -1); 
      break;
    case 'q': 
      stepParameter('t', -1); 
      break;
    case 'e': 
      stepParameter('t', 1); 
      break;
    case ' ': 
      if (animate) {
        animate = false;
        noLoop();
      } 
      else {
        animate = true;
        time_delta = millis() - time;
        loop();
      }
      break;
    }
  }
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "java_triangle_world" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
