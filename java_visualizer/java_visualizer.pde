import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioInput in;
FFT fft;

int padding = 100; //padding applied left and right in px
int padding_bottom = 100;
float height_scaling = 10;
float depth_scaling = .25; //how big is the las spectrum compared to the first one?
PVector depth_offset = new PVector(0, 500);
int history_length = 50;
int start_color = 0;
int end_color = 255;
int avg_length = 4; //timeframe to move the graphs and average values for coming graph 

float[][] history;
int upperFreqBand;


void setup()
{
  size(displayWidth, displayHeight, P2D);
  frameRate(60);
  smooth();

  minim = new Minim(this);

  //get AudioInput, buffersize has to be a power of 2, fft spectrum will be half as big as the covered timeframe
  in = minim.getLineIn(minim.STEREO, 2048, 192000);
  fft = new FFT(in.bufferSize(), in.sampleRate());
  
  upperFreqBand = fft.freqToIndex(20000);

  history = new float[history_length][upperFreqBand];
  for (int i = 0; i < history_length; i++) {
    for (int j = 0; j < upperFreqBand; j++) {
      history[i][j] = 0;
    }
  }
}


void draw()
{
  background(255);
  println((int) frameRate + "fps " + frameCount);

  fft.forward(in.mix);

  //update history
  for (int i = history_length - 1; i >= 0; i--) {
    for (int j = 0; j < upperFreqBand; j++) {
      if (i > 0) history[i][j] = history[i - 1][j];
      else history[i][j] = fft.getBand(j);
    }
  }
  
  //draw spectrums
   for(int i = history_length - 1; i >= 0; i--) {
     float depth_percentage = (float) i / (history_length - 1);
     stroke( lerp(start_color, end_color, depth_percentage) );
     float offset_left = (float) padding + lerp(0, (width - width * depth_scaling) / 2., depth_percentage) 
       + lerp(0, depth_offset.x, depth_percentage);
     float offset_top = (float) height - padding_bottom - lerp(0, depth_offset.y, depth_percentage) * lerp(1, depth_scaling, depth_percentage);
     float step_size = (float) (width - offset_left * 2) / upperFreqBand;
     float line_scaling = height_scaling * lerp(1, depth_scaling, depth_percentage);
     for(int j = 1; j < upperFreqBand; j++) {
       line(offset_left + step_size * (j - 1), offset_top - history[i][j - 1] * line_scaling, 
         offset_left + step_size * (j), offset_top - history[i][j] * line_scaling);
     }
   }
   
   fill(0);
   text("lowest middle freq: " + fft.indexToFreq(0) + "Hz, highest used middle freq: " + fft.indexToFreq(upperFreqBand) + " Hz, upperFreqBand: " + upperFreqBand, 50, 50);
}

void stop() {
  in.close();
  minim.stop();
}

void keyPressed()
{
  if ( key == 'm' || key == 'M' )
  {
    if ( in.isMonitoring() )
    {
      in.disableMonitoring();
    } else
    {
      in.enableMonitoring();
    }
  }
}

