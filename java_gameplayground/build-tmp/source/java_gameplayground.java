import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class java_gameplayground extends PApplet {

float dist_x = 3;
float dist_y = 3;
float size_x = 110;
float size_y = 70;
float scaling_noise_height = 100;
float scaling_noise_area = 0.01f;
long map_seed = (long)random(0, 1000000000);
Camera cam;

public void setup() {
	size(displayWidth, displayHeight, P3D);
	fill(204);
	stroke(64);
	strokeWeight(.5f);
	noiseSeed(map_seed);
	cam = new Camera();
}

public void draw() {
	println(scaling_noise_area, "; ", frameCount, " frames so far, ", frameRate, " fps");
	background(204);
	lights();
	cam.draw();
	drawGrid(cam.center.x, cam.center.y);
}

public void drawGrid(float middle_x, float middle_y) {
	float tmp_x, tmp_y_1, tmp_y_2;
	float half_dist_x = dist_x / 2;
	// fix grid to ground
	middle_x = middle_x - middle_x % dist_x;
	middle_y = middle_y - middle_y % (dist_y * 2);
	
	for (int y = 1; y <= size_y; y++) {
		beginShape(TRIANGLE_STRIP);
		tmp_y_1 = middle_y + (size_y * dist_y / 2) - y * dist_y;
		tmp_y_2 = tmp_y_1 + dist_y;
		tmp_x = middle_x - size_x * dist_x / 2;

		vertex(tmp_x, tmp_y_2, noise(tmp_x, tmp_y_2, map_seed));
		tmp_x += half_dist_x;
		for (int x = 1; x <= size_x; x++) {
			if (y % 2 == 0) {
				vertex(tmp_x, tmp_y_1, noise(tmp_x * scaling_noise_area, tmp_y_1 * scaling_noise_area) * scaling_noise_height);
				tmp_x += half_dist_x;
				vertex(tmp_x, tmp_y_2, noise(tmp_x * scaling_noise_area, tmp_y_2 * scaling_noise_area) * scaling_noise_height);
				tmp_x += half_dist_x;
			} else {
				vertex(tmp_x, tmp_y_2, noise(tmp_x * scaling_noise_area, tmp_y_2 * scaling_noise_area) * scaling_noise_height);
				tmp_x += half_dist_x;
				vertex(tmp_x, tmp_y_1, noise(tmp_x * scaling_noise_area, tmp_y_1 * scaling_noise_area) * scaling_noise_height);
				tmp_x += half_dist_x;
			}
		}
		endShape();
	}
}

public void keyPressed() {
	if (key == CODED) {
		if (keyCode == UP)
			cam.target.y += 10;
		else if (keyCode == DOWN)
			cam.target.y -= 10;
		else if (keyCode == LEFT)
			cam.target.x += 10;
		else if (keyCode == RIGHT)
			cam.target.x -= 10;
	}
}

public void mouseDragged() {
	if (keyPressed && key == CODED && keyCode == ALT) {

	} else {
		cam.target.x += (pmouseX - mouseX) * 0.5f;
		cam.target.y += (pmouseY - mouseY) * 0.5f;
	}
}

public void mouseWheel(MouseEvent event) {
	scaling_noise_area += event.getCount() * scaling_noise_area * 0.1f;
}

class Camera {
	PVector target, position;
	private PVector center;

	Camera() {
		target = new PVector(0, 0, 0); // where the camera should look
		center = new PVector(0, 0, 0); // where the cam is actually looking
		position = new PVector(0, 70, 220); // relative to center
	}

	public void draw() {
		center = PVector.lerp(center, target, 0.25f);
		camera(center.x + position.x, 
		center.y + position.y, 
		center.z + position.z, 
		center.x, center.y, center.z, 0, 0, -1);
		//perspective(camera_fov, width/height, camera_z / 100, camera_z * 100);
		perspective();
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "java_gameplayground" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
