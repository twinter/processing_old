Walker p1, p2, p3, p4;

void setup() {
  size(1024, 768, P2D);
  background(255);
  fill(0, 255, 0);
  stroke(0);
  
  p1 = new Walker(10, 10);
  p2 = new Walker(10, 100, 1, 0);
  p3 = new Walker(100, 10, 2, 0);
  p4 = new Walker(100, 100, 3, 0);
}

void draw() {
  //background(255);
  //triangle(width/2, height/2, p1.getX(), p1.getY(), p2.getX(), p2.getY());
  quad(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY(), p4.getX(), p4.getY());
}

class Walker {
  float x;
  float y;
  float x_delta = 0.1;
  float y_delta = 0.1;
  float x_force = 0;
  float y_force = 0.025;
  
  Walker(float x_pos, float y_pos) {
    x = x_pos;
    y = y_pos;
  }
  
  Walker(float x_pos, float y_pos, float x_dir, float y_dir) {
    x = x_pos;
    y = y_pos;
    x_delta = x_dir;
    y_delta = y_dir;
  }
  
  float getX() {
    updateX();
    return x;
  }
  
  void updateX() {
    if (x > width || x < 0) {
      x_delta = x_delta * -1;
    }
    x = x + x_delta;
    x_delta = x_delta + x_force;
  }
  
  float getY() {
    updateY();
    return y;
  }
  
  void updateY() {
    if (y > height || y < 0) {
      y_delta = y_delta * -1;
    }
    y = y + y_delta;
    y_delta = y_delta + y_force;
  }
}














