import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class java_walker extends PApplet {

Walker p1, p2, p3, p4;

public void setup() {
  size(1024, 768, P2D);
  background(255);
  fill(0, 255, 0);
  stroke(0);
  
  p1 = new Walker(10, 10);
  p2 = new Walker(10, 100, 1, 0);
  p3 = new Walker(100, 10, 2, 0);
  p4 = new Walker(100, 100, 3, 0);
}

public void draw() {
  //background(255);
  //triangle(width/2, height/2, p1.getX(), p1.getY(), p2.getX(), p2.getY());
  quad(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY(), p4.getX(), p4.getY());
}

class Walker {
  float x;
  float y;
  float x_delta = 0.1f;
  float y_delta = 0.1f;
  float x_force = 0;
  float y_force = 0.025f;
  
  Walker(float x_pos, float y_pos) {
    x = x_pos;
    y = y_pos;
  }
  
  Walker(float x_pos, float y_pos, float x_dir, float y_dir) {
    x = x_pos;
    y = y_pos;
    x_delta = x_dir;
    y_delta = y_dir;
  }
  
  public float getX() {
    updateX();
    return x;
  }
  
  public void updateX() {
    if (x > width || x < 0) {
      x_delta = x_delta * -1;
    }
    x = x + x_delta;
    x_delta = x_delta + x_force;
  }
  
  public float getY() {
    updateY();
    return y;
  }
  
  public void updateY() {
    if (y > height || y < 0) {
      y_delta = y_delta * -1;
    }
    y = y + y_delta;
    y_delta = y_delta + y_force;
  }
}














  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "java_walker" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
