class Node {
  private ArrayList<Node> parents;
  private ArrayList<Node> children;
  private int pos_x, pos_y;
  private float hue;
  private Graph graph;

  Node(Graph graph, int x, int y) {
    parents = new ArrayList<Node>();
    children = new ArrayList<Node>();
    this.graph = graph;
    pos_x = x;
    pos_y = y;
    hue = -1;
  }

  public int getX() {
    return pos_x;
  }

  public int getY() {
    return pos_y;
  }

  public color getColor() {
    if (hue < 0)
      return color(255);
    else
      return color(hue, 255, 255);
  }
  
  public float getHue() {
    return hue;
  }
  
  public void setHue(float hue) {
    this.hue = hue;
    graph.registerChangedNode(this);
  }

  public void resetColor() {
    this.hue = -1;
    graph.registerChangedNode(this);
  }

  public boolean isLonely() {
    if (this.hasChildren())
      return false;
    if (!this.isOrphan())
      return false;
    return true;
  }

  public boolean isOrphan() {
    if (parents == null || parents.size() == 0)
      return true;
    return false;
  }



  public boolean hasChildren() {
    if (children == null || children.size() == 0)
      return false;
    return true;
  }

  public void addChild(Node new_child) {
    children.add(new_child);
    new_child.addParent(this);
    graph.registerChangedNode(this);
  }

  public ArrayList<Node> getChildren() {
    return children;
  }

  public Node getChild(int x, int y) {
    for (int i = 0; i < children.size(); i++) {
      Node n = children.get(i);
      if (x == n.getX() && y == n.getY()) {
        return n;
      }
    }
    return null;
  }

  public boolean removeChild(int x, int y) {
    int i = 0;
    while (i < children.size()) {
      Node n = children.get(i);
      if (x == n.getX() && y == n.getY()) {
        children.remove(i);
        n.removeParent(this.pos_x, this.pos_y);
        graph.registerChangedNode(this);
        return true;
      }
      i++;
    }
    return false;
  }
  
  public ArrayList<Node> getParents() {
    return parents;
  }
  
  public Node getFirstParent() {
    if (parents.size() > 0)
      return parents.get(0);
    else
      return null;
  }



  // do not use these externaly as it would fuck up the double linked tree of nodes
  public void addParent(Node new_parent) {
    parents.add(new_parent);
    graph.registerChangedNode(this);
  }

  public boolean removeParent(int x, int y) {
    for (int i = parents.size() - 1; i >= 0; i--) {
      Node n = parents.get(i);
      if (n.getX() == x && n.getY() == y) {
        parents.remove(i);
        graph.registerChangedNode(this);
        return true;
      }
    }
    return false;
  }
}