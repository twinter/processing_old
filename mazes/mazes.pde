/*
generate mazes
exchangable generators and walkers

maybe also seperate the functions for drawing
*/

/*
TODO
increase hue on solvers according to previous node
*/

MazeHandler mh;

void setup() {
  //frameRate(240);
  //size(1024, 768, P2D);
  fullScreen(P2D);
  colorMode(HSB);
  noStroke();
  fill(128);
  background(0);
  
  Graph graph = new Graph(2, 1);
  mh = new MazeHandler(graph, 500);
  
  //GenRandom gen = new GenRandom(graph);
  GenRandomDFT gen = new GenRandomDFT(graph);
  
  //SolverDFT slv = new SolverDFT(graph);
  SolverBFT slv = new SolverBFT(graph);
  
  slv.setIncSize(0.25);
  
  mh.setGenerator(gen);
  mh.setSolver(slv);
}

void draw() {
  println(mh.getTodoSize(), " ", frameRate);
  mh.update();
}

void mouseClicked() {
  mh.reset();
}

void keyPressed() {
  if (key == ' ')
    mh.finishStage();
}