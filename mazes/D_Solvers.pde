class Solver extends GraphTraverser {
  protected float h_inc = 1;

  Solver(Graph graph) { 
    init(graph);
  }

  public void setIncSize(float inc_size) {
    h_inc = inc_size;
  }

  protected void incrementHue(Node n) {
    Node parent = n.getFirstParent();
    float h_new = 0; //new hue
    if (parent != null)
      h_new = parent.getHue() + h_inc;
    while(h_new > 255)
      h_new -= 255;
    n.setHue(h_new);
  }
}






//depth-first traversal
class SolverDFT extends Solver {
  SolverDFT(Graph graph) { 
    super(graph);
  }

  public void step() {
    if (!this.isDone()) {
      //get last item in todo and color it
      Node current = todo.get(todo.size() - 1);
      this.incrementHue(current);

      //remove current node from todo and add all Children to todo
      todo.remove(todo.size() - 1);
      this.addTodo(current.getChildren());
    }
  }
}



//breath-first traversal
class SolverBFT extends Solver {
  SolverBFT(Graph graph) { 
    super(graph);
  }

  public void step() {
    if (!this.isDone()) {
      //get first item in todo and color it
      Node current = todo.get(0);
      this.incrementHue(current);

      //remove current node from todo and add all children to todo
      todo.remove(0);
      this.addTodo(current.getChildren());
    }
  }
}