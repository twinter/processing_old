//this is just meant as an interface and has no actual function except as a parent for other classes
class GraphTraverser {
  protected ArrayList<Node> todo;
  protected Graph graph;

  //children have to replace this
  public void step() {}

  public boolean isDone() {
    return !(todo.size() > 0);
  }

  public int getTodoSize() {
    return todo.size();
  }
  
  public void reset() {
    todo = new ArrayList<Node>();
    todo.add(graph.getFirstNode());
  }

  protected void init(Graph graph) {
    this.graph = graph;
    todo = new ArrayList<Node>();
    todo.add(graph.getFirstNode());
  }

  protected void addTodo(ArrayList<Node> nodes) {
    for (int i = 0; i < nodes.size(); i++) {
      this.addTodo(nodes.get(i));
    }
  }

  protected void addTodo(Node node) {
    if (!todo.contains(node))
      todo.add(node);
  }
}