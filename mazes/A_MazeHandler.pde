class MazeHandler {
  int step_size = 1; //how many steps should be done for each call of update()

  private Graph graph = null;
  private Generator generator = null;
  private Solver solver = null;
  private boolean generator_done = false;
  private boolean solver_done = false;

  MazeHandler(Graph graph) {
    this.graph = graph;
  }

  MazeHandler(Graph graph, int step_size) {
    this.graph = graph;
    this.step_size = step_size;
  }

  public void update() {
    graph.draw();
    if (!generator_done) {
      runGenerator();
    } else if (!solver_done) {
      runSolver();
    }
  }

  public void finishStage() {
    if (!generator_done)
      while (runGenerator());
    else if (!solver_done)
      while (runSolver());
  }

  public void reset() {
    graph.reset();
    if (generator != null)
      generator.reset();
    if (solver != null)
      solver.reset();
    generator_done = false;
    solver_done = false;
  }
  
  public int getTodoSize() {
    if (generator != null) {
      if (!generator_done)
        return generator.getTodoSize();
      else if (solver != null && solver_done == false)
        return solver.getTodoSize();
    }
    return 0;
  }

  public void setGenerator(Generator gen) {
    generator = gen;
  }

  public void setSolver(Solver slv) {
    solver = slv;
  }

  public Graph getGraph() {
    return graph;
  }

  public Generator getGenerator() {
    return generator;
  }

  public Solver getSolver() {
    return solver;
  }



  private boolean runGenerator() {
    if (generator != null) {
      if (generator.isDone() == false) {
        for (int i=0; i<step_size; i++)
          generator.step();
        return true;
      } else {
        generator_done = true;
      }
    }
    return false;
  }
  
  private boolean runSolver() {
    if (solver != null) {
      if (solver.isDone() == false) {
        for (int i=0; i<step_size; i++)
          solver.step();
        return true;
      } else {
        solver_done = true;
      }
    }
    return false;
  }
}