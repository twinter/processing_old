//this is just meant as an interface and has no actual function except as a parent for other classes
class Generator extends GraphTraverser {
  Generator(Graph graph) {
    init(graph);
  }
}



//random depth-first traversal
class GenRandomDFT extends Generator {
  Node current;

  GenRandomDFT(Graph graph) {
    super(graph);
    current = todo.get(0);
  }
  
  public void reset() {
    super.reset();
    current = todo.get(0);
  }

  public void step() {
    ArrayList<Node> neighbours = graph.getLonelyNeighbours(current);

    //if there are no orphan neighbours: continue with a random node from todo
    if (neighbours.size() == 0) {
      if (todo.size() > 0) {
        Node next_current = todo.get( floor(random(todo.size())) );
        todo.remove(current);
        current = next_current;
      }
      return;
    }

    Node next = neighbours.get( floor(random(neighbours.size())) ); //chose a random neighbour
    current.addChild(next); //connect it with the current node
    addTodo(next); //add it to todo
    current = next; //advance
  }
}



//simple random traversal
class GenRandom extends Generator {
  GenRandom(Graph graph) { super(graph); }

  public void step() {
    if (todo.size() > 0) {
      //get a random node from todo
      int element_nr = floor(random(todo.size()));
      Node n = todo.get(element_nr);

      //get lonely neighbours and add the to the todo list
      ArrayList<Node> neighbours = graph.getLonelyNeighbours(n);

      if (neighbours.size() > 0) {
        //if there is an orphan neighbour: connect it and add to todo
        Node child = neighbours.get( floor(random(neighbours.size())) );
        n.addChild(child);
        addTodo(child);
      } else {
        //no orphan neighbour: remove node from todo
        todo.remove(element_nr);
      }
    }
  }
}