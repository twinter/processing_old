class Graph {
  private Node[][] nodes;
  private int size_node = 10;
  private int size_wall = 5;
  private ArrayList<Node> changelist = new ArrayList<Node>();

  Graph() {
    init();
  }

  Graph(int size_node, int size_wall) {
    this.size_node = size_node;
    this.size_wall = size_wall;
    init();
  }

  private void init() {
    int length_x = floor((width - size_wall) / (size_node + size_wall));
    int length_y = floor((height - size_wall) / (size_node + size_wall));
    nodes = new Node[length_x][length_y];
    changelist.clear();
    background(0);

    for (int x = 0; x < length_x; x++) {
      for (int y = 0; y < length_y; y++) {
        nodes[x][y] = new Node(this, x, y);
        changelist.add(nodes[x][y]);
      }
    }

    println(length_x * length_y + " node, x:" + length_x + " y:" + length_y);
  }

  public void reset() {
    init();
  }

  void draw() {
    rectMode(CORNER);
    int distance = size_node + size_wall;

    for (Node n : changelist) {
      if (n.isLonely())
        fill(128);
      else
        fill(n.getColor());

      rect(size_wall + n.getX() * distance, size_wall + n.getY() * distance, size_node, size_node);

      //draw connections to children
      if (size_wall > 0)
        this.drawConnections(n, distance);
    }
    
    changelist.clear();
  }

  /**
   * @param  node      the node whose children are to be drawn
   * @param  distance  size_wall + size_node, just to avoid extra work
   */
  private void drawConnections(Node node, int distance) {
    ArrayList<Node> children = node.getChildren();
    int nx = node.getX();
    int ny = node.getY();
    fill(node.getColor());

    for (int i = 0; i < children.size(); i++) {
      Node c = children.get(i);
      //fill(c.getColor());

      int cx = c.getX();
      int cy = c.getY();

      if (cx == nx) {
        if (cy == ny - 1) //child above
          rect(size_wall + nx * distance, ny * distance, size_node, size_wall);
        else if (cy == ny + 1) //child below
          rect(size_wall + nx * distance, size_wall + ny * distance + size_node, size_node, size_wall);
      } else if (cy == ny) {
        if (cx == nx - 1) //child left
          rect(nx * distance, size_wall + ny * distance, size_wall, size_node);
        else if (cx == nx + 1) //child right
          rect(size_wall + nx * distance + size_node, size_wall + ny * distance, size_wall, size_node);
      }
    }
  }



  public void registerChangedNode(Node node) {
    if (!changelist.contains(node))
      changelist.add(node);
  }

  public Node getFirstNode() {
    return nodes[0][0];
  }

  public Node getNode(int x, int y) {
    return nodes[x][y];
  }

  public ArrayList<Node> getOrphanNeighbours(Node n) {
    return this.getOrphanNeighbours(n.getX(), n.getY());
  }

  public ArrayList<Node> getOrphanNeighbours(int x, int y) {
    ArrayList<Node> n = this.getNeighbours(x, y);
    if (n == null)
      return null;

    for (int i = n.size() - 1; i >= 0; i--) {
      if (!n.get(i).isOrphan())
        n.remove(i);
    }
    return n;
  }

  public ArrayList<Node> getLonelyNeighbours(Node n) {
    return this.getLonelyNeighbours(n.getX(), n.getY());
  }

  public ArrayList<Node> getLonelyNeighbours(int x, int y) {
    ArrayList<Node> n = this.getNeighbours(x, y);
    if (n == null)
      return null;

    for (int i = n.size() - 1; i >= 0; i--) {
      if (!n.get(i).isLonely())
        n.remove(i);
    }
    return n;
  }

  public ArrayList<Node> getNeighbours(Node n) {
    return this.getNeighbours(n.getX(), n.getY());
  }

  public ArrayList<Node> getNeighbours(int x, int y) {
    if (x < 0 || y < 0 || x > nodes.length-1 || y > nodes[0].length-1)
      return null;

    ArrayList<Node> n = new ArrayList<Node>(4);

    //top
    if (x > 0)
      n.add(nodes[x-1][y]);

    //below
    if (x < nodes.length - 1)
      n.add(nodes[x+1][y]);

    //left
    if (y > 0)
      n.add(nodes[x][y-1]);

    //
    if (y < nodes[0].length - 1)
      n.add(nodes[x][y+1]);

    return n;
  }
}