

float[] a1 = {-1, -1}; // anchor
float[] c1 = {-1, -1}; // control
float[] a2 = {-1, -1};
float[] c2 = {-1, -1};
int r = 10; // radius for points in pixels
float speed = 15; // maximum movement per cycle in pixels
float tmp; // temporary storage
 
void setup() {
  size(800, 600);
  background(0);
  colorMode(RGB);
  stroke(255);
}

void draw() {
  fill(255, 16);
  rect(0, 0, width, height);
  noStroke();
  if (a1[0] >= 0) {
    fill(255, 0, 0);
    ellipse(a1[0], a1[1], r, r);
  }
  if (a2[0] >= 0) {
    ellipse(a2[0], a2[1], r, r);
  }
  if (c1[0] >= 0) {
    fill(0, 0, 255);
    ellipse(c1[0], c1[1], r, r);
  }
  if (c2[0] >= 0) {
    ellipse(c2[0], c2[1], r, r);
    stroke(0, 128);
    noFill();
    bezier(a1[0], a1[1], c1[0], c1[1], c2[0], c2[1], a2[0], a2[1]);
    
    tmp = random(-1 * PI, PI);
    a1[0] += sin(tmp) * speed;
    a1[1] += cos(tmp) * speed;
    tmp = random(-1 * PI, PI);
    a2[0] += sin(tmp) * speed;
    a2[1] += cos(tmp) * speed;
    tmp = random(-1 * PI, PI);
    c1[0] += sin(tmp) * speed;
    c1[1] += cos(tmp) * speed;
    tmp = random(-1 * PI, PI);
    c2[0] += sin(tmp) * speed;
    c2[1] += cos(tmp) * speed;
    
    
  }
  
}

float[] movePoint2D(float[] point)

void mouseClicked() {
  if (a1[0] < 0) {
    a1[0] = mouseX;
    a1[1] = mouseY;
  } else if (c1[0] == -1) {
    c1[0] = mouseX;
    c1[1] = mouseY;
  } else if (a2[0] == -1) {
    a2[0] = mouseX;
    a2[1] = mouseY;
  } else if (c2[0] == -1) {
    c2[0] = mouseX;
    c2[1] = mouseY;
  } else {
    a1[0] = -1;
    c1[0] = -1;
    a2[0] = -1;
    c2[0] = -1;
  }
}

