void setup() {
  size(500, 500, P2D);
  smooth(8);
  noStroke();
  result = new int[width*height][3];
  result_ = new int[width*height][3];
}

int[][] result, result_;
float time;

void draw_() {
  if (aberrationAmount == 0.0) {
    draw__();
    return;
  }

  for (int i=0; i<width*height; i++)
    for (int a=0; a<3; a++)
      result_[i][a] = 0;

  for (int a=0; a<3; a++) {
    pushMatrix();
    translate(width/2, height/2);
    scale(1+0.008*a*aberrationAmount);
    translate(-width/2, -height/2);
    draw__();
    popMatrix();
    loadPixels();
    for (int i=0; i<pixels.length; i++) {
      result_[i][a] = pixels[i] >> (8*(2-a)) & 0xff;
    }
  }

  loadPixels();
  for (int i=0; i<pixels.length; i++)
    pixels[i] = 0xff << 24 | result_[i][0] << 16 | 
      result_[i][1] << 8 | result_[i][2];
  updatePixels();
}

void draw() {
  if (shutterAngle == 0.0) {
    time = map(frameCount-1, 0, numFrames, 0, 1) % 1;
    draw_();
    return;
  }

  for (int i=0; i<width*height; i++)
    for (int a=0; a<3; a++)
      result[i][a] = 0;

  for (int sa=0; sa<samplesPerFrame; sa++) {
    time = map(frameCount-1 + sa*shutterAngle/samplesPerFrame, 0, numFrames, 0, 1);
    draw_();
    loadPixels();
    for (int i=0; i<pixels.length; i++) {
      result[i][0] += pixels[i] >> 16 & 0xff;
      result[i][1] += pixels[i] >> 8 & 0xff;
      result[i][2] += pixels[i] & 0xff;
    }
  }

  loadPixels();
  for (int i=0; i<pixels.length; i++)
    pixels[i] = 0xff << 24 | (result[i][0]/samplesPerFrame) << 16 | 
      (result[i][1]/samplesPerFrame) << 8 | (result[i][2]/samplesPerFrame);
  updatePixels();

  saveFrame("f###.gif");
  if (frameCount==numFrames)
    exit();
}

////////////////////////////////////////////////////

float aberrationAmount = .4;

int samplesPerFrame = 4;
int numFrames = 132;        
float shutterAngle = .75;

float l = 30, sp = 2*l, t, tt;
float x, y;
float c1 = 26, c2 = 240;
int N = 7;

float ease(float p){
  float g = 3*p*p - 2*p*p*p;
  g = 3*g*g - 2*g*g*g;
  return 3*g*g - 2*g*g*g;
}

void draw__() {
  t = time;
  background(62);

  pushMatrix();
  translate(width/2, height/2);
  rotate(QUARTER_PI);
  
  if (t <= .275) {
    tt = map(t,0,.275,0,1);
    tt = ease(tt);
    for (int i=-N; i<=N; i++) {
      for (int j=-N; j<=N; j++) {
        for (int a=0; a<4; a++) {
          x = 0+i*sp; 
          y = 0+j*sp;

          if (a<2)
            x += .5*tt*sp;
          else
            x -= .5*tt*sp;
          pushMatrix();
          fill((i+j)%2 == 0 ? c1 : c2);
          translate(x, y);
          rotate(HALF_PI*a);
          rect(0, 0, l/2, l/2);
          popMatrix();
        }
      }
    }
  }
  else if (t <= 0.55) {
    tt = map(t,.275,.55,0,1);
    tt = ease(tt);
    for (int i=-N; i<=N; i++) {
      for (int j=-N; j<=N; j++) {
        for (int a=0; a<4; a++) {
          x = 0+(i+.5)*sp; 
          y = 0+j*sp;

          if (a==0||a==3)
            y += .5*tt*sp;
          else
            y -= .5*tt*sp;
          pushMatrix();
          fill((i+j+a/2)%2 == 0 ? c1 : c2);
          translate(x, y);
          rotate(HALF_PI*a);
          rect(0, 0, l/2, l/2);
          popMatrix();
        }
      }
    }
  }

  else if (t <= 0.775) {
    tt = map(t,0.55,0.775,0,1);
    tt = ease(tt);
    for (int i=-N; i<=N; i++) {
      for (int j=-N; j<=N; j++) {
        for (int a=0; a<4; a++) {
          x = 0+(i+.5)*sp; 
          y = 0+(j+.5)*sp;

          if (a==0||a==3)
            x += .25*tt*sp;
          else
            x -= .25*tt*sp;
          pushMatrix();
          fill((i+j+a)%2 == 0 ? c1 : c2);
          translate(x, y);
          rotate(HALF_PI*a);
          rect(0, 0, l/2, l/2);
          popMatrix();
        }
      }
    }
  }

  else {
    tt = map(t,0.775,1,0,1);
    tt = ease(tt);
    for (int i=-N; i<=N; i++) {
      for (int j=-N; j<=N; j++) {
        for (int a=0; a<4; a++) {
          x = 0+i*sp; 
          y = 0+(j+.5)*sp;

          if (a<2)
            y += .25*tt*sp;
          else
            y -= .25*tt*sp;
          pushMatrix();
          fill((i+j+a/2+1)%2 == 0 ? c1 : c2);
          translate(x, y);
          rotate(HALF_PI*a);
          rect(0, 0, l/2, l/2);
          popMatrix();
        }
      }
    }
  }
  popMatrix();
}